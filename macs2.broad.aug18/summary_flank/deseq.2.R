file_matrix<-commandArgs(trailingOnly = TRUE)[1]
file_pheno<-commandArgs(trailingOnly = TRUE)[2]
num_sample<-commandArgs(trailingOnly = TRUE)[3]
num_sample<-as.integer(num_sample)
prefix<-commandArgs(trailingOnly = TRUE)[4]

library(DESeq2)
countData<-as.matrix(read.csv(file_matrix, row.names="locus", sep="\t", header=T))
colData<-read.table(file_pheno, sep="\t", row.names=1, header=T)
#colData$group<-factor(paste0(colData$day, colData$state))
dds<-DESeqDataSetFromMatrix(countData=countData, colData=colData, design=~1)
dds<-estimateSizeFactors(dds)

write.table(file=paste0("sizefactors.", prefix, ".txt", sep=""), dds$sizeFactor, sep="\t", quot=F)
