import sys
import os
import re

def read_phenotype():
	pheno = {}
	pheno["K4me3_CC"] = ["k4me3"]
	pheno["K4me3_CY"] = ["k4me3"]
	pheno["K4me3_HC"] = ["k4me3"]
	pheno["K4me3_HY"] = ["k4me3"]
	return pheno

def read_list(n):
	f = open(n)
	aList = []
	for l in f:
		l = l.rstrip("\n")
		#ll = l.split("\t")
		aList.append(l.split(".")[0])
	f.close()
	return aList

if __name__=="__main__":
	fname = sys.argv[1]
	out_prefix = sys.argv[2]
	os.system("cat %s|xargs paste|awk \"NR%%2==0\" > /tmp/1" % fname)
	os.system("cat `head -n 1 %s`|awk \"NR%%2==1\" > /tmp/h" % fname)
	os.system("paste /tmp/h /tmp/1 > /tmp/1.h")
	os.system("echo 'locus'|cat - %s|tr '\n' '\t'|sed 's/\t$/\\n/g' > /tmp/2" % fname)
	os.system("cat /tmp/2 /tmp/1.h > /tmp/3")

	pheno = read_phenotype()
	aList = read_list(fname)
	fw = open("/tmp/pheno", "w")
	fw.write("condition\tstate\n")
	for al in aList:
		fw.write("%s\t%s\n" % (al, pheno[al][0]))
	fw.close()

	os.system("Rscript deseq.2.R /tmp/3 /tmp/pheno %d %s" % (len(aList), out_prefix))
