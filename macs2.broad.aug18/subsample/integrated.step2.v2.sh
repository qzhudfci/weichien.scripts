#!/bin/bash
#SBATCH -n 1                               # Request one core
#SBATCH -N 1                               # Request one node (if you request more than one core with -n, also using
                                           # -N 1 means all cores will be on the same node)
#SBATCH -t 0-12:00                         # Runtime in D-HH:MM format
#SBATCH -p short                           # Partition to run in
#SBATCH --mem=32000                        # Memory total in MB (for all cores)
#SBATCH -o hostname_%j.out                 # File to which STDOUT will be written, including job ID
#SBATCH -e hostname_%j.err                 # File to which STDERR will be written, including job ID
#SBATCH --mail-type=ALL                    # Type of email notification- BEGIN,END,FAIL,ALL
#SBATCH --mail-user=bernardzhu@gmail.com   # Email to which notifications will be sent

Rscriptbin=/n/app/R/3.3.3/bin
pythonbin=/n/app/python/2.7.12/bin
bedopsbin=/n/app/bedops/2.4.30
picardbin=/n/app/picard/2.8.0/bin
picardjarfile=picard-2.8.0.jar
samtoolsbin=/n/app/samtools/1.3.1/bin
macs2bin=/n/app/macs2/2.1.1.20160309/bin
javabin=/n/app/java/jdk-1.8u112/bin
extratoolsbin=/home/qz64/cutrun_pipeline
extrasettings=/home/qz64/cutrun_pipeline
chromsizedir=`dirname /home/qz64/chrom.mm10/mm10.fa`
macs2pythonlib=/n/app/macs2/2.1.1.20160309/lib/python2.7/site-packages

pythonlib=`echo $PYTHONPATH | tr : "\n" | grep -v $macs2pythonlib | paste -s -d:`
unset PYTHONPATH
export PYTHONPATH=$macs2pythonlib:$pythonlib

pythonldlibrary=/n/app/python/2.7.12/lib
ldlibrary=`echo $LD_LIBRARY_PATH | tr : "\n" | grep -v $pythonldlibrary | paste -s -d:`
unset LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$pythonldlibrary:$ldlibrary


>&2 echo "Input parameters are: $1"
>&2 date

#expand the path of $1
relinfile=`realpath -s $1`
dirname=`dirname $relinfile`
base=`basename $1 .bam`

#cd to current directory (aligned.aug10)
cd $dirname

workdir=`pwd`
logdir=$workdir/logs_p0.05

>&2 echo "Peak calling using MACS2... ""$base".bam
>&2 echo "Logs are stored in $logdir"
>&2 date
bam_file="$base".bam
dir=`dirname $bam_file`
base_file=`basename $bam_file .bam`
outdir=$workdir/macs2 #for macs2

for d in $outdir $logdir; do
if [ ! -d $d ]; then
mkdir $d
fi
done

#$macs2bin/macs2 callpeak -t $workdir/$dir/"$base_file".bam -g mm -f BAM -n $base_file --outdir $outdir --keep-dup all -B -p 0.05 2> $logdir/"$base_file".macs2
$macs2bin/macs2 callpeak -t $workdir/$dir/"$base_file".bam -g mm -f BAMPE -n $base_file --outdir $outdir -q 0.01 -B --keep-dup all 2> $logdir/"$base_file".macs2

#cur=`pwd`
>&2 echo "Converting bedgraph to bigwig... ""$base".bam
>&2 date
cd $outdir
LC_ALL=C sort -k1,1 -k2,2n $outdir/"$base_file"_treat_pileup.bdg > $outdir/"$base_file".sort.bdg
$extratoolsbin/bedGraphToBigWig $outdir/"$base_file".sort.bdg $chromsizedir/mm10.chrom.sizes $outdir/"$base_file".sorted.bw
rm -rf "$base_file".sort.bdg

>&2 echo "Finished"
>&2 date

