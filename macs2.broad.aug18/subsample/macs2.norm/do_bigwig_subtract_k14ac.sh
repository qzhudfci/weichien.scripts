#!/bin/bash
#SBATCH -n 1                               # Request one core
#SBATCH -N 1                               # Request one node (if you request more than one core with -n, also using
                                           # -N 1 means all cores will be on the same node)
#SBATCH -t 0-12:00                         # Runtime in D-HH:MM format
#SBATCH -p short                           # Partition to run in
#SBATCH --mem=32000                        # Memory total in MB (for all cores)
#SBATCH -o hostname_%j.out                 # File to which STDOUT will be written, including job ID
#SBATCH -e hostname_%j.err                 # File to which STDERR will be written, including job ID
#SBATCH --mail-type=ALL                    # Type of email notification- BEGIN,END,FAIL,ALL
#SBATCH --mail-user=bernardzhu@gmail.com   # Email to which notifications will be sent

bigwigCompare -b1 K14ac_CC.sorted.bw -b2 Input_CC.sorted.bw --operation subtract --outFileName K14ac_CC_subtract.bw
bigwigCompare -b1 K14ac_CY.sorted.bw -b2 Input_CY.sorted.bw --operation subtract --outFileName K14ac_CY_subtract.bw
bigwigCompare -b1 K14ac_HC.sorted.bw -b2 Input_HC.sorted.bw --operation subtract --outFileName K14ac_HC_subtract.bw
bigwigCompare -b1 K14ac_HY.sorted.bw -b2 Input_HY.sorted.bw --operation subtract --outFileName K14ac_HY_subtract.bw
