#!/bin/bash
#SBATCH -n 1                               # Request one core
#SBATCH -N 1                               # Request one node (if you request more than one core with -n, also using
                                           # -N 1 means all cores will be on the same node)
#SBATCH -t 0-5:00                         # Runtime in D-HH:MM format
#SBATCH -p short                           # Partition to run in
#SBATCH --mem=8000                        # Memory total in MB (for all cores)
#SBATCH -o hostname_%j.out                 # File to which STDOUT will be written, including job ID
#SBATCH -e hostname_%j.err                 # File to which STDERR will be written, including job ID
#SBATCH --mail-type=ALL                    # Type of email notification- BEGIN,END,FAIL,ALL
#SBATCH --mail-user=bernardzhu@gmail.com   # Email to which notifications will be sent

module load samtools
samtools view -s 0.9999 -b bam/Input_CC_21_L6_aligned_reads.bam > subsample/Input_CC.bam
samtools view -s 0.7142 -b bam/Input_CY_31_L6_aligned_reads.bam > subsample/Input_CY.bam
samtools view -s 0.8064 -b bam/Input_HC_27_L1_aligned_reads.bam > subsample/Input_HC.bam
samtools view -s 0.9615 -b bam/Input_HY_42_L1_aligned_reads.bam > subsample/Input_HY.bam
