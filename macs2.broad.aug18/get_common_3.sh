#!/bin/bash
file1=$1
file2=$2
file3=$3

bedops -e 1 $file1 $file2 |bedops -e 1 - $file3 > /tmp/ov.bed
bedops -e 1 $file1 $file2 |bedops -n 1 - $file3 > /tmp/ov12not3.bed
bedops -e 1 $file1 $file3 |bedops -n 1 - $file2 > /tmp/ov13not2.bed
bedops -e 1 $file2 $file3 |bedops -n 1 - $file1 > /tmp/ov23not1.bed
bedops -n 1 $file1 $file2 |bedops -n 1 - $file3 > /tmp/1sub2sub3.bed
bedops -n 1 $file2 $file1 |bedops -n 1 - $file3 > /tmp/2sub1sub3.bed
bedops -n 1 $file3 $file1 |bedops -n 1 - $file2 > /tmp/3sub1sub2.bed

bedops -u /tmp/ov.bed /tmp/ov12not3.bed /tmp/ov13not2.bed /tmp/ov23not1.bed /tmp/1sub2sub3.bed /tmp/2sub1sub3.bed /tmp/3sub1sub2.bed > $4
