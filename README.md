### Processing scripts for WeiChien's Chip-seq samples


### Table of contents

CUT&RUNTools was used to process paired-end Chip-seq samples.

#### CUT&RUNTools environment

[current_config.json](current_config.json) lists the pre-requisite package environment settings. 

#### Trimming, alignment scripts

Trimmomatic was used with default settings. Default bowtie2 settings were used. Exact commands can be located in the file [integrated.sh](integrated.sh).

#### BAM file sorting and mark duplicates

The script for sorting bam file (samtools) and marking duplicates (Picard) is found in [aligned.aug10/integrated.step2.sh](aligned.aug10/integrated.step2.sh).

#### Normalization

Chip-seq samples (TF antibody-based) are normalized using a background-based procedure, which ensures that background read levels are same across all normalized samples. Background is defined as 10,000bp region surrounding peaks (minus any peak regions that it may overlap with). Background reads are used to generate a scale-factor per sample for subsampling (next step).

#### Subsampling of reads based on scale factor

[aligned.aug10/dup.marked/subsample.hbo1.sh](aligned.aug10/dup.marked/subsample.hbo1.sh)

[aligned.aug10/dup.marked/subsample.yap.sh](aligned.aug10/dup.marked/subsample.yap.sh)

[aligned.aug10/dup.marked/subsample.tead4.sh](aligned.aug10/dup.marked/subsample.tead4.sh)

[aligned.aug10/dup.marked/subsample.spikeinfree.k14ac.sh](aligned.aug10/dup.marked/subsample.k14ac.sh)

[aligned.aug10/dup.marked/subsample.spikeinfree.k27ac.sh](aligned.aug10/dup.marked/subsample.k27ac.sh)

[aligned.aug10/dup.marked/subsample.spikeinfree.k4me3.sh](aligned.aug10/dup.marked/subsample.k4me3.sh)


### Peak calling

On subsampled BAM files, MACS2 default broadpeak settings were used to call peaks. See file for exact setting: [aligned.aug10/dup.marked/subsample/macs2.broad.sh](aligned.aug10/dup.marked/subsample/macs2.broad.sh).




