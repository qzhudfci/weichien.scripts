#!/bin/bash
#SBATCH -n 4                               # Request one core
#SBATCH -N 1                               # Request one node (if you request more than one core with -n, also using
                                           # -N 1 means all cores will be on the same node)
#SBATCH -t 0-12:00                          # Runtime in D-HH:MM format
#SBATCH -p short                           # Partition to run in
#SBATCH --mem=16000                        # Memory total in MB (for all cores)
#SBATCH -o hostname_%j.out                 # File to which STDOUT will be written, including job ID
#SBATCH -e hostname_%j.err                 # File to which STDERR will be written, including job ID
#SBATCH --mail-type=ALL                    # Type of email notification- BEGIN,END,FAIL,ALL
#SBATCH --mail-user=bernardzhu@gmail.com   # Email to which notifications will be sent

module load samtools
base=`basename $1 _aligned_reads.bam`

mkdir sorted dedup dup.marked dedup.150bp dup.marked.150bp dup.marked.gt.150bp

samtools view -h "$base"_aligned_reads.bam |head -n 1000|grep "^@"|sed "s/unsorted/coordinate/g" > "$base"_aligned_reads.header

>&2 echo "mark duplicate..."
time LC_ALL=C samtools view -@ 2 -h "$base"_aligned_reads.bam|~/bin/samblaster --ignoreUnmated|LC_ALL=C samtools view -@ 2 -Sb -f 3 -F 4 -F 8 - > "$base"_markdup.bam

>&2 echo "Sort by coordinate..."
time samtools view -@ 2 "$base"_markdup.bam|LC_ALL=C sort -t"	" -T . -S 8G --compress-program=gzip --parallel=2 -k3,3 -k4,4n |cat "$base"_aligned_reads.header -|samtools view -@ 2 -Sb - > dup.marked/"$base"_aligned_reads.bam

>&2 echo "Get dup.marked.150bp version..."
time samtools view -@ 2 -h dup.marked/"$base"_aligned_reads.bam |LC_ALL=C awk -f ~/cutrun_pipeline/filter_below_150bp.awk |samtools view -@ 2 -Sb - > dup.marked.150bp/"$base"_aligned_reads.bam

>&2 echo "Get dup.marked above 150bp version..."
time samtools view -@ 2 -h dup.marked/"$base"_aligned_reads.bam |LC_ALL=C awk -f filter_above.awk |samtools view -@ 2 -Sb - > dup.marked.gt.150bp/"$base"_aligned_reads.bam

>&2 echo "Get dedup version..."
time samtools view -@ 2 -bh -F 1024 dup.marked/"$base"_aligned_reads.bam > dedup/"$base"_aligned_reads.bam

#>&2 echo "Get dedup.150bp version..."
#time samtools view -@ 2 -bh -F 1024 dup.marked.150bp/"$base"_aligned_reads.bam > dedup.150bp/"$base"_aligned_reads.bam

