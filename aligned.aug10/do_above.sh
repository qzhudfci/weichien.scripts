#!/bin/bash
#SBATCH -n 4                               # Request one core
#SBATCH -N 1                               # Request one node (if you request more than one core with -n, also using
                                           # -N 1 means all cores will be on the same node)
#SBATCH -t 0-12:00                          # Runtime in D-HH:MM format
#SBATCH -p short                           # Partition to run in
#SBATCH --mem=16000                        # Memory total in MB (for all cores)
#SBATCH -o hostname_%j.out                 # File to which STDOUT will be written, including job ID
#SBATCH -e hostname_%j.err                 # File to which STDERR will be written, including job ID
#SBATCH --mail-type=ALL                    # Type of email notification- BEGIN,END,FAIL,ALL
#SBATCH --mail-user=bernardzhu@gmail.com   # Email to which notifications will be sent

Rscriptbin=/n/app/R/3.3.3/bin
pythonbin=/n/app/python/2.7.12/bin
bedopsbin=/n/app/bedops/2.4.30
picardbin=/n/app/picard/2.8.0/bin
picardjarfile=picard-2.8.0.jar
samtoolsbin=/n/app/samtools/1.3.1/bin
macs2bin=/n/app/macs2/2.1.1.20160309/bin
javabin=/n/app/java/jdk-1.8u112/bin
extratoolsbin=/home/qz64/cutrun_pipeline
extrasettings=/home/qz64/cutrun_pipeline
chromsizedir=`dirname /home/qz64/chrom.hg19/hg19.fa`
macs2pythonlib=/n/app/macs2/2.1.1.20160309/lib/python2.7/site-packages

pythonlib=`echo $PYTHONPATH | tr : "\n" | grep -v $macs2pythonlib | paste -s -d:`
unset PYTHONPATH
export PYTHONPATH=$macs2pythonlib:$pythonlib

pythonldlibrary=/n/app/python/2.7.12/lib
ldlibrary=`echo $LD_LIBRARY_PATH | tr : "\n" | grep -v $pythonldlibrary | paste -s -d:`
unset LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$pythonldlibrary:$ldlibrary

base=`basename $1 _aligned_reads.bam`


mkdir dup.marked.gt.150bp

#>&2 echo "Get dup.marked above 150bp version..."
#time $samtoolsbin/samtools view -@ 2 -h dup.marked/"$base"_aligned_reads.bam |LC_ALL=C awk -f filter_above.awk |samtools view -@ 2 -Sb - > dup.marked.gt.150bp/"$base"_aligned_reads.bam
#samtools index dup.marked.gt.150bp/"$base"_aligned_reads.bam

workdir=`pwd`
logdir=$workdir/logs
bam_file=dup.marked.gt.150bp/"$base".bam
dir=`dirname $bam_file`
base_file=`basename $bam_file .bam`

outdir=$workdir/../macs2.broad.aug18.gt.150bp #for macs2

for d in $outdir $logdir; do
if [ ! -d $d ]; then
mkdir $d
fi
done

#$macs2bin/macs2 callpeak -t $workdir/$dir/"$base"_aligned_reads.bam -g mm -f BAMPE -n $base_file --outdir $outdir -q 0.01 -B --SPMR --keep-dup all 2> $logdir/"$base_file".gt.150bp.macs2
$macs2bin/macs2 callpeak -t $workdir/$dir/"$base"_aligned_reads.bam -g mm -f BAMPE -n $base_file --outdir $outdir --broad --broad-cutoff 0.1 -B --SPMR --keep-dup all 2> $logdir/"$base_file".gt.150bp.macs2
