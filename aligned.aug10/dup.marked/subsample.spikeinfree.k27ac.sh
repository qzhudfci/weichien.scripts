#!/bin/bash
#SBATCH -n 1                               # Request one core
#SBATCH -N 1                               # Request one node (if you request more than one core with -n, also using
                                           # -N 1 means all cores will be on the same node)
#SBATCH -t 0-5:00                         # Runtime in D-HH:MM format
#SBATCH -p short                           # Partition to run in
#SBATCH --mem=8000                        # Memory total in MB (for all cores)
#SBATCH -o hostname_%j.out                 # File to which STDOUT will be written, including job ID
#SBATCH -e hostname_%j.err                 # File to which STDERR will be written, including job ID
#SBATCH --mail-type=ALL                    # Type of email notification- BEGIN,END,FAIL,ALL
#SBATCH --mail-user=bernardzhu@gmail.com   # Email to which notifications will be sent
module load samtools

if [ ! -d "subsample.spikeinfree" ]
then
mkdir subsample.spikeinfree
fi
samtools view -s 0.81301 -b K27ac_CC_19_L6_aligned_reads.bam > subsample.spikeinfree/K27ac_CC.bam
samtools view -s 0.59172 -b K27ac_CY_28_L6_aligned_reads.bam > subsample.spikeinfree/K27ac_CY.bam
samtools view -s 1.00000 -b K27ac_HC_25_L1_aligned_reads.bam > subsample.spikeinfree/K27ac_HC.bam
samtools view -s 0.78740 -b K27ac_HY_35_L1_aligned_reads.bam > subsample.spikeinfree/K27ac_HY.bam
