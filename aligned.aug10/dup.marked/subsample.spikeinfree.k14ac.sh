#!/bin/bash
#SBATCH -n 1                               # Request one core
#SBATCH -N 1                               # Request one node (if you request more than one core with -n, also using
                                           # -N 1 means all cores will be on the same node)
#SBATCH -t 0-5:00                         # Runtime in D-HH:MM format
#SBATCH -p short                           # Partition to run in
#SBATCH --mem=8000                        # Memory total in MB (for all cores)
#SBATCH -o hostname_%j.out                 # File to which STDOUT will be written, including job ID
#SBATCH -e hostname_%j.err                 # File to which STDERR will be written, including job ID
#SBATCH --mail-type=ALL                    # Type of email notification- BEGIN,END,FAIL,ALL
#SBATCH --mail-user=bernardzhu@gmail.com   # Email to which notifications will be sent
module load samtools

if [ ! -d "subsample.spikeinfree" ]
then
mkdir subsample.spikeinfree
fi
samtools view -s 0.60804 -b K14ac_CC_14_L6_aligned_reads.bam > subsample.spikeinfree/K14ac_CC.bam
samtools view -s 1.00000 -b K14ac_CY_23_L6_aligned_reads.bam > subsample.spikeinfree/K14ac_CY.bam
samtools view -s 0.50251 -b K14ac_HC_20_L1_aligned_reads.bam > subsample.spikeinfree/K14ac_HC.bam
samtools view -s 0.77387 -b K14ac_HY_29_L1_aligned_reads.bam > subsample.spikeinfree/K14ac_HY.bam
