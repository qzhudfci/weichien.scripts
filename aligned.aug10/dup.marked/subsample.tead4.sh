#!/bin/bash
#SBATCH -n 1                               # Request one core
#SBATCH -N 1                               # Request one node (if you request more than one core with -n, also using
                                           # -N 1 means all cores will be on the same node)
#SBATCH -t 0-5:00                         # Runtime in D-HH:MM format
#SBATCH -p short                           # Partition to run in
#SBATCH --mem=8000                        # Memory total in MB (for all cores)
#SBATCH -o hostname_%j.out                 # File to which STDOUT will be written, including job ID
#SBATCH -e hostname_%j.err                 # File to which STDERR will be written, including job ID
#SBATCH --mail-type=ALL                    # Type of email notification- BEGIN,END,FAIL,ALL
#SBATCH --mail-user=bernardzhu@gmail.com   # Email to which notifications will be sent
module load samtools

if [ ! -d "subsample" ]
then
mkdir subsample
fi
samtools view -s 0.94076 -b TEAD4_CC_15_L6_aligned_reads.bam > subsample/TEAD4_CC.bam
samtools view -s 0.93673 -b TEAD4_CY_24_L6_aligned_reads.bam > subsample/TEAD4_CY.bam
samtools view -s 1.00000 -b TEAD4_HC_21_L1_aligned_reads.bam > subsample/TEAD4_HC.bam
samtools view -s 0.66363 -b TEAD4_HY_31_L1_aligned_reads.bam > subsample/TEAD4_HY.bam
