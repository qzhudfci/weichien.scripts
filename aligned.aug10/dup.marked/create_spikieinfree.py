import sys
import re
import os

f = open(sys.argv[1])
bams = []
f.readline()
for l in f:
	l = l.rstrip("\n")
	bams.append(l.split("\t")[0])
f.close()

m_file = sys.argv[1]
fw = open(sys.argv[2], "w")
fw.write("library(\"ChIPseqSpikeInFree\")\n")
fw.write("metaFile <- \"%s\"\n" % m_file)
fw.write("bams <- c(\"%s\", \"%s\", \"%s\", \"%s\")\n" % (bams[0], bams[1], bams[2], bams[3]))
fw.write("ChIPseqSpikeInFree(bamFiles=bams, chromFile=\"mm10\", metaFile=metaFile, prefix=\"%s\")" % sys.argv[3])
fw.close()
