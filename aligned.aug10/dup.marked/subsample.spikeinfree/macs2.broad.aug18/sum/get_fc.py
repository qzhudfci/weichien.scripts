import numpy as np
import os
import re
import sys

def read(n1, n2):
	f1 = open(n1)
	f2 = open(n2)
	m = []
	while True:
		l1 = f1.readline()
		l2 = f2.readline()
		if l1=="": break
		if l2=="": break
		l1 = l1.rstrip("\n")
		l2 = l2.rstrip("\n")

		c1 = int(f1.readline().rstrip("\n"))
		c2 = int(f2.readline().rstrip("\n"))
		m.append((c1, c2))
	f1.close()
	f2.close()
	return m

if __name__=="__main__":
	#m = read("HBO1_CY_at_YAP.sum", "HBO1_CC_at_YAP.sum")
	m = read("HBO1_CY_at_nonYAP.sum", "HBO1_CC_at_nonYAP.sum")
	nx = []
	for i,j in m:
		#fc = np.log2(float(i+1) / float(j+1))
		#fc = np.log2(float(i+1))
		#fc = np.log2(float(j+1))
		#print(i,j, (i-j))
		fc = i - j
		nx.append(fc)
	nx = np.array(nx)
	print(10, np.percentile(nx, 10))
	print(20, np.percentile(nx, 20)) 
	print(30, np.percentile(nx, 30)) 
	print(40, np.percentile(nx, 40)) 
	print(50, np.percentile(nx, 50)) 
	print(60, np.percentile(nx, 60)) 
	print(70, np.percentile(nx, 70)) 
	print(80, np.percentile(nx, 80))
	print(90, np.percentile(nx, 90)) 
	print("mean", np.mean(nx), np.std(nx)) 
