#!/usr/bin/python

import sys
import re
import os
from operator import itemgetter
for l in sys.stdin:
	l = l.rstrip("\n")
	ll = l.split("\t")

	mapTo = l.split("|")[1].split(";")

	t_list = []
	for lm in mapTo:
		ll = lm.split("\t")
		c1,c2,c3 = ll[0],ll[1],ll[2] #chr, start, end
		t_id = ll[3]
		val = float(ll[4])
		t_list.append((c1, c2, c3, t_id, val))

	t_list.sort(key=itemgetter(4), reverse=True)
	t_item = t_list[0]

	sys.stdout.write("%s\t%s\t%s\t%s\t%.5f\n" % (t_item[0], t_item[1], t_item[2], t_item[3], t_item[4]))
