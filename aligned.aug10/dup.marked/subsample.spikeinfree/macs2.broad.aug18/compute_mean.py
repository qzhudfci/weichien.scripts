import sys
import os
import re
import numpy as np
import json

def read(n):
	f = open(n)
	setting = f.readline().rstrip("\n")[1:]
	ss = json.loads(setting)
	print(ss)

	num_row = 0
	num_col = 0	
	for l in f:
		l = l.rstrip("\n")
		if num_row==0:
			ll = l.split("\t")
			#print(len(ll))
			num_col = len(ll)-6
		num_row+=1
	f.close()
	#print(num_row)
	mat = np.empty((num_row, num_col), dtype="float32")
	rows = []

	f = open(n)
	f.readline()
	ic = 0
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		rows.append(ll[3])
		mat[ic] = [float(v) for v in ll[6:]]
		ic+=1
	f.close()

	rows = np.array(rows)
	mat_sum = np.sum(mat, axis=1)
	#print(mat_sum.shape)

	good_ids = []
	for ic in range(mat_sum.shape[0]):
		#print(ic, mat_sum[ic])
		'''
		if np.isnan(mat_sum[ic]):
			continue
		vv = np.log2(mat_sum[ic])
		if vv>15.5:
			continue
		'''
		good_ids.append(ic)
	
	good_ids = np.array(good_ids)
	rows = rows[good_ids]
	mat = mat[good_ids,:]

	#print(good_ids.shape)	
	#'group_boundaries': [0, 7367]	
	ss["group_boundaries"] = [0, good_ids.shape[0]]
	return mat, rows, ss

if __name__=="__main__":
	mat, rows, ss = read(sys.argv[1])
	means = np.mean(mat, axis=0)
	tx = ss["sample_boundaries"][1]
	n_means = np.empty((tx, len(ss["sample_labels"])), dtype="float32")
	for i_r,val in enumerate(ss["sample_boundaries"]):
		t_next = ss["sample_boundaries"][i_r+1]
		#print(means[val:t_next])
		n_means[:,i_r] = means[val:t_next]
		if i_r==len(ss["sample_boundaries"])-2:
			break

	fw = open(sys.argv[2], "w")
	fw.write("X\t" + "\t".join(ss["sample_labels"]) +"\n")
	for ix in range(n_means.shape[0]):
		i_x = -2000 + ix*10
		fw.write("%d\t" % i_x + "\t".join(["%.6f" % v for v in n_means[ix,:]]) + "\n")
	fw.close()
