import sys
import os
import re
import numpy as np
import json

def read(n, cutoff=15):
	f = open(n)
	setting = f.readline().rstrip("\n")[1:]
	ss = json.loads(setting)
	print(ss)
	num_row = 0
	num_col = 0	
	for l in f:
		l = l.rstrip("\n")
		if num_row==0:
			ll = l.split("\t")
			num_col = len(ll)-6
		num_row+=1
	f.close()
	mat = np.empty((num_row, num_col), dtype="float32")
	rows = []

	f = open(n)
	f.readline()
	ic = 0
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		rows.append(ll[3])
		mat[ic] = [float(v) for v in ll[6:]]
		ic+=1
	f.close()

	rows = np.array(rows)
	mat_sum = np.sum(mat, axis=1)

	good_ids = []
	for ic in range(mat_sum.shape[0]):
		print(ic, mat_sum[ic])
		if np.isnan(mat_sum[ic]):
			continue
		vv = np.log2(mat_sum[ic])
		if vv>cutoff:
			continue
		good_ids.append(ic)
	
	good_ids = np.array(good_ids)
	rows = rows[good_ids]
	mat = mat[good_ids,:]

	#print(good_ids.shape)	
	#'group_boundaries': [0, 7367]	
	ss["group_boundaries"] = [0, good_ids.shape[0]]
	return mat, rows, ss

if __name__=="__main__":
	mat, rows, ss = read(sys.argv[1], cutoff=14.8)

	
	fw = open(sys.argv[2], "w")
	fw.write("@" + json.dumps(ss) + "\n")
	for ir, i_row in enumerate(rows):
		c1 = rows[ir].split(":")[0]
		c2,c3 = rows[ir].split(":")[1].split("-")
		
		fw.write("%s\t%s\t%s\t%s\t.\t.\t" % (c1, c2, c3, rows[ir]))
		fw.write("\t".join(["%.6f" % v for v in mat[ir,:]]) + "\n")
	fw.close()
	
