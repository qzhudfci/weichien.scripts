plotHeatmap -m matrix1_k14ac_hy_cy_comparison_peaks.gz -out matrix1_k14ac_hy_cy_peaks.png --whatToShow "heatmap and colorbar" --colorMap Blues Blues
plotHeatmap -m matrix1_k14ac_hc_cc_comparison_peaks.gz -out matrix1_k14ac_hc_cc_peaks.png --whatToShow "heatmap and colorbar" --colorMap Blues Blues
