#!/bin/bash
file1=$1
file2=$2

bedops -e 1 $file1 $file2 > /tmp/ov.bed
bedops -n 1 $file1 $file2 > /tmp/1sub2.bed
bedops -n 1 $file2 $file1 > /tmp/2sub1.bed

bedops -u /tmp/ov.bed /tmp/1sub2.bed /tmp/2sub1.bed > $3
