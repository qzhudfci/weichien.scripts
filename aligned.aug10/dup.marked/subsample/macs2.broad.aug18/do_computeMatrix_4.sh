#!/bin/bash
#SBATCH -n 1                               # Request one core
#SBATCH -N 1                               # Request one node (if you request more than one core with -n, also using
                                           # -N 1 means all cores will be on the same node)
#SBATCH -t 0-5:00                         # Runtime in D-HH:MM format
#SBATCH -p short                           # Partition to run in
#SBATCH --mem=64000                        # Memory total in MB (for all cores)
#SBATCH -o hostname_%j.out                 # File to which STDOUT will be written, including job ID
#SBATCH -e hostname_%j.err                 # File to which STDERR will be written, including job ID
#SBATCH --mail-type=ALL                    # Type of email notification- BEGIN,END,FAIL,ALL
#SBATCH --mail-user=bernardzhu@gmail.com   # Email to which notifications will be sent

module load samtools
module load deeptools
t_bed=YAP_CY_peaks.summits.bed
computeMatrix reference-point --referencePoint TSS -b 2000 -a 2000 -R $t_bed -S K14ac_HC.sorted.bw K14ac_CC.sorted.bw -o matrix1_k14ac_hc_cc_comparison_peaks.gz
t_bed=YAP_HY_peaks.summits.bed
computeMatrix reference-point --referencePoint TSS -b 2000 -a 2000 -R $t_bed -S K14ac_HY.sorted.bw K14ac_CY.sorted.bw -o matrix1_k14ac_hy_cy_comparison_peaks.gz
