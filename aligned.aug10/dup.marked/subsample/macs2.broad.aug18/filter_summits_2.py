#!/usr/bin/python

import sys
import re
import os
from operator import itemgetter
for l in sys.stdin:
	l = l.rstrip("\n")
	ll = l.split("\t")

	ref_id = ll[3]
	mapTo = l.split("|")[1].split(";")

	t_list_cc = []
	t_list_cy = []
	t_list_hc = []
	t_list_hy = []
	for lm in mapTo:
		ll = lm.split("\t")
		c1,c2,c3 = ll[0],ll[1],ll[2] #chr, start, end
		t_id = ll[3]
		val = float(ll[4])
		if "YAP_CC" in ref_id:
			t_list_cc.append((c1, c2, c3, t_id, val))
		if "YAP_CY" in ref_id:
			t_list_cy.append((c1, c2, c3, t_id, val))
		if "YAP_HC" in ref_id:
			t_list_hc.append((c1, c2, c3, t_id, val))
		if "YAP_HY" in ref_id:
			t_list_hy.append((c1, c2, c3, t_id, val))

	t_list_cc.sort(key=itemgetter(4), reverse=True)
	t_list_cy.sort(key=itemgetter(4), reverse=True)
	t_list_hc.sort(key=itemgetter(4), reverse=True)
	t_list_hy.sort(key=itemgetter(4), reverse=True)

	
	if "YAP_CC" in ref_id:
		t_item = t_list_cc[0]
	if "YAP_CY" in ref_id:
		t_item = t_list_cy[0]
	if "YAP_HC" in ref_id:
		t_item = t_list_hc[0]
	if "YAP_HY" in ref_id:
		t_item = t_list_hy[0]
	sys.stdout.write("%s\t%s\t%s\t%s\t%.5f\n" % (t_item[0], t_item[1], t_item[2], t_item[3], t_item[4]))
