#!/bin/bash
#SBATCH -n 1                               # Request one core
#SBATCH -N 1                               # Request one node (if you request more than one core with -n, also using
                                           # -N 1 means all cores will be on the same node)
#SBATCH -t 0-5:00                         # Runtime in D-HH:MM format
#SBATCH -p short                           # Partition to run in
#SBATCH --mem=64000                        # Memory total in MB (for all cores)
#SBATCH -o hostname_%j.out                 # File to which STDOUT will be written, including job ID
#SBATCH -e hostname_%j.err                 # File to which STDERR will be written, including job ID
#SBATCH --mail-type=ALL                    # Type of email notification- BEGIN,END,FAIL,ALL
#SBATCH --mail-user=bernardzhu@gmail.com   # Email to which notifications will be sent

module load samtools
module load deeptools
#original normalization
#computeMatrix reference-point --referencePoint TSS -b 2000 -a 2000 -R YAP_CY_filtered_peaks.summits.bed -S YAP_CC.sorted.bw YAP_HC.sorted.bw TEAD4_CC.sorted.bw TEAD4_HC.sorted.bw -o matrix1_yap_cy_peaks_YAP_CC_HC_TEAD4_CC_HC.gz

#spikeinfree
d1=../../subsample.spikeinfree/macs2.broad.aug18
computeMatrix reference-point --referencePoint TSS -b 2000 -a 2000 -R YAP_CY_filtered_peaks.summits.bed -S $d1/YAP_CC.sorted.bw $d1/YAP_HC.sorted.bw $d1/TEAD4_CC.sorted.bw $d1/TEAD4_HC.sorted.bw -o matrix1_spikeinfree_yap_cy_peaks_YAP_CC_HC_TEAD4_CC_HC.gz
#computeMatrix reference-point --referencePoint TSS -b 2000 -a 2000 -R YAP_CY_filtered_peaks.summits.bed -S YAP_CY.sorted.bw YAP_CC.sorted.bw TEAD4_CY.sorted.bw TEAD4_CC.sorted.bw $d1/K14ac_CY.sorted.bw $d1/K14ac_CC.sorted.bw HBO1_CY.sorted.bw HBO1_CC.sorted.bw -o matrix1_jun7_yap_cy_peaks.gz
