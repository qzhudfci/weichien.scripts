#!/usr/bin/python

import sys
import re
import os
from operator import itemgetter
for l in sys.stdin:
	l = l.rstrip("\n")
	ll = l.split("\t")

	mapTo = l.split("|")[1].split(";")

	t_list_yap = []
	t_list_tead = []
	for lm in mapTo:
		ll = lm.split("\t")
		c1,c2,c3 = ll[0],ll[1],ll[2] #chr, start, end
		t_id = ll[3]
		val = float(ll[4])
		if "TEAD" in t_id:
			t_list_tead.append((c1, c2, c3, t_id, val))
		if "YAP" in t_id:
			t_list_yap.append((c1, c2, c3, t_id, val))

	t_list_yap.sort(key=itemgetter(4), reverse=True)
	t_list_tead.sort(key=itemgetter(4), reverse=True)

	if len(t_list_yap)>=1 and len(t_list_tead)>=1:
		t_item_yap = t_list_yap[0]
		t_item_tead = t_list_tead[0]
		sys.stdout.write("%s\t%s\t%s\t%s\t%.5f\n" % (t_item_yap[0], t_item_yap[1], t_item_yap[2], t_item_yap[3], t_item_yap[4]))
