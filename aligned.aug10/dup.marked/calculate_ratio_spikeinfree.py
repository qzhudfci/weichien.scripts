import sys
import os
import re
from operator import itemgetter

def read_depth(n):
	f = open(n)
	m = {}
	bam = {}
	for l in f:
		l = l.rstrip("\n")
		if l.startswith("Doing"):
			t_sample = l.split()[1]
			tx = t_sample.split("_")
			new_sample = tx[0] + "_" + tx[1]
			count = int(f.readline().rstrip("\n").split()[0])
			m[new_sample] = count
			bam[new_sample] = t_sample
	f.close()
	print(m)
	return m, bam

def read_ratio(n):
	f = open(n)
	m = {}
	f.readline()
	to_inverse = False
	for l in f:
		l = l.rstrip("\n")
		ll = l.split("\t")
		t_sample = ll[0]
		tx = t_sample.split("_")
		new_sample = tx[0] + "_" + tx[1]
		if ll[-1]!="NA":
			#m[new_sample] = 1.0 / float(ll[-1])
			m[new_sample] = float(ll[-1])
		else:
			m[new_sample] = -1.0
			to_inverse = True
	f.close()
	if to_inverse:
		for t_key in m:
			if m[t_key]==-1.0:
				m[t_key] = 1.0
				continue
			m[t_key] = 1.0 / m[t_key]
		
		#t_min = min([m[t_key] for t_key in m])
		#for t_key in m:
		#	m[t_key] = m[t_key] / t_min
	return m

def get_header():
	m1 = """#!/bin/bash
#SBATCH -n 1                               # Request one core
#SBATCH -N 1                               # Request one node (if you request more than one core with -n, also using
                                           # -N 1 means all cores will be on the same node)
#SBATCH -t 0-5:00                         # Runtime in D-HH:MM format
#SBATCH -p short                           # Partition to run in
#SBATCH --mem=8000                        # Memory total in MB (for all cores)
#SBATCH -o hostname_%j.out                 # File to which STDOUT will be written, including job ID
#SBATCH -e hostname_%j.err                 # File to which STDERR will be written, including job ID
#SBATCH --mail-type=ALL                    # Type of email notification- BEGIN,END,FAIL,ALL
#SBATCH --mail-user=bernardzhu@gmail.com   # Email to which notifications will be sent
module load samtools
"""
	return m1

if __name__=="__main__":
	m,bam = read_depth(sys.argv[1]) #hostname_**.out file (containing output of samtools flagstat)
	ratio = read_ratio(sys.argv[2]) #output of deseq.2.R which is sizefactors file

	it_ratio = list(ratio.items())
	it_ratio.sort(key=itemgetter(1))
	print(it_ratio)
	ref = it_ratio[0][0]

	factor = {}
	for t_key in ratio:
		t_rat = ratio[t_key]/ratio[ref]
		inv = 1.0/t_rat
		new_size = int(inv * m[t_key])
		print(t_key, ratio[t_key], ratio[t_key]/ratio[ref], inv, m[t_key], new_size)
		factor[t_key] = inv

	fw = open(sys.argv[3], "w") #output subsample script
	outdir = "subsample.spikeinfree"
	m1 = get_header()
	fw.write(m1 + "\n")
	fw.write("if [ ! -d \"%s\" ]\n" % outdir)
	fw.write("then\n")
	fw.write("mkdir %s\n" % outdir)
	fw.write("fi\n")

	for t_key in factor:
		fw.write("samtools view -s %.5f -b %s > %s/%s.bam\n" % (factor[t_key], bam[t_key], outdir, t_key))
	fw.close()

	os.system("chmod a+x %s" % sys.argv[3])
